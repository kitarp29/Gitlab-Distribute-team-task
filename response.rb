require 'net/http'

def measure_response_time(url, start_time, retries=3) # Retry counter here

    response = Net::HTTP.get_response(URI(url))


    if response.is_a?(Net::HTTPSuccess)
      end_time = Time.now
      response_time = end_time - start_time
      return response_time
    end
    if retries > 0
      puts "Request failed! Retrying..."
      return measure_response_time(url, start_time, retries - 1)
    end

  return nil
end

url = "https://about.gitlab.com/"  # We could replace it with https://gitlab.com but it will fail!
start_time = Time.now

response_time = measure_response_time(url, start_time)

if response_time
  if response_time > 300
    puts "Response time is #{response_time} seconds (exceeds threshold)"
  else
    puts "Response time is #{response_time} seconds (within threshold)"
  end
else
  puts "Request failed even after retries"
end